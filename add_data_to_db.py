"""
Created on Sat Jan 20 13:06:10 2018

@author: foromodanielsoromou
"""
import json
import boto3
import uuid

s3 = boto3.resource('s3')
dynamodb = boto3.resource('dynamodb')
#file = s3.Object('mtl-data', 'fact_mtl.json')
file = s3.Object('mtl-data', 'question_count.json')
table = dynamodb.Table('mtl_questions')
#table = dynamodb.Table('mtl_facts')


def lambda_handler(event, context):
    questions = json.loads(file.get()['Body'].read().decode('utf-8'))
    print(questions)
    for ques in questions:
        ques['id'] = str(uuid.uuid4())
        response = table.put_item(Item=ques)
    return "Done"
