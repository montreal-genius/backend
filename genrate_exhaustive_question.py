#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 12:13:28 2018

@author: foromodanielsoromou
"""

import json

filename = "./lieux_culturels_municipaux_montreal.json"

def number_of_occurence(json_data, category):
    cpt= 0
    for item in json_data:
        if item['Noms du réseau'].find(category) !=  -1 :
            cpt += 1
    return cpt


def generate_question_by_categorie(json_data, list_of_categories):

    array_of_question = []
    array_of_anwser= []
    
    for category in list_of_categories:
        n = number_of_occurence(json_data, category)
        question = {
                'question' : "How many {} are there in montreal?".format(category),
                'answer' : n,
                'category' : 'culture',
                'answer_choices': []
                }
        array_of_question.append(question)
        array_of_anwser.append(n)
        
    for question in array_of_question:
        for ans in array_of_anwser:
            if ans not in question['answer_choices']:
                question['answer_choices'].append(ans)
    
    return array_of_question

def generate_question_address_by_place(json_data):
    
    array_of_question = []
    array_of_anwser= []
    
    for place in json_data:
        for category in place['Noms du réseau'].split(','):
            p = place['Adresse'].split(',')
            str = "at"
            ad = p[0]
            if len(p) > 1 :
                str ="on"
                ad = p[1]
                
            
            question = {
                    'question' : "what {} is {} the {}?".format(category, str, ad),
                    'answer' : place['Nom du lieu culturel municipal'],
                    'category' : 'culture',
                    'answer_choices': []
                    }
            array_of_question.append(question)
            array_of_anwser.append(place['Nom du lieu culturel municipal'])
            
    for question in array_of_question:
        for ans in array_of_anwser:
            if ans not in question['answer_choices']:
                question['answer_choices'].append(ans)
                
    return array_of_question

list_of_categorie = []

json_data = None
with open(filename) as json_file:
    json_data = json.load(json_file)
    
    for item in json_data:
        for subitem in item['Noms du réseau'].split(','):
            list_of_categorie.append(subitem)

list_of_categorie = set(list_of_categorie)

question = generate_question_by_categorie(json_data, list_of_categorie)
question.extend(generate_question_address_by_place(json_data))
print(question)
