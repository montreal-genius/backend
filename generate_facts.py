#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 21 02:18:58 2018

@author: foromodanielsoromou
"""

import boto3
import json
import uuid
import random
from boto3.dynamodb.conditions import Key, Attr

s3 = boto3.resource('s3')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('mtl_facts')

def lambda_handler(event, context):
    if not event['params']['querystring'] or not event['params']['querystring']['category']:
        return {"message" : "some parameters missing"}
    
    category = event['params']['querystring']['category']
    number_of_fact = int(event['params']['querystring']['n'])
    
    response = None
    if category == 'all':
        response = table.scan()
    else:
        response = table.scan(FilterExpression=Key('category').eq(event['params']['querystring']['category']))
    data = response['Items']

    while 'LastEvaluatedKey' in response:
        if category == 'all':
            response = table.scan()
        else:
            response = table.scan(
                FilterExpression=Key('category').eq(event['params']['querystring']['category']),
                ExclusiveStartKey=response['LastEvaluatedKey']
            )
        data.extend(response['Items'])
    
    history = []
    
    facts = []
    
  
    if len(data) > 0:
        for i in range(number_of_fact):
            
            choice =  random.randint(0, len(data) - 1)
            while choice in history:
                choice =  random.randint(0, len(data) - 1)
            
            facts.append(data[choice])
            history.append(choice)
    
    return facts
